public class Student{
	private String school;
	private double rScore;
	private int age;
	public int amountLearnt;
	
	public Student(String school, double rScore){
		this.school = school;
		this.rScore = rScore;
		this.age = 18;
		this.amountLearnt = 0;
		
	}
	
	public void judgeAcademicPerformance (double rScore){
		if (rScore == 25.0) {
			System.out.println("You have pretty average grades");
		}
		else if (rScore > 25.0) {
			System.out.println("You're good, Please do my homework");
		}
		else if (rScore < 15.0) {
			System.out.println("Are you a tourist or a student?");
		}
		else {
			System.out.println("You have to work a bit harder...");
		}
		
	}
	public void judgeAge () {
		if (age > 30) {
			System.out.println("How many grades did you repeat??");
		}
		else {
			System.out.println("You've got your whole life ahead of you");
		}
	
	
	}
	public void learn(int amountStudied) {
		if(amountStudied > 0) {
			
			this.amountLearnt =  this.amountLearnt+amountStudied;
		}
	}
	public String getSchool() {
		return this.school;
	}
	public double getRscore() {
		return this.rScore;
	}
	public int getAge() {
		return this.age;
	}
	public void setAge(int newAge) {
		this.age = newAge;
	}
	
}